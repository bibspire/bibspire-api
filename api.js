const {bibHandler} = require('./bibdata/bib');
const yaml = require('js-yaml');
const {promisify} = require('util');
const fs = require('fs');
const readFile = promisify(fs.readFile);
const {accessLogStats} = require('./accesslog');
const {geoip} = require('./misc/geoip');

//
// Swagger
//
function swaggerUI(req, res) {
  res.send(`
<!DOCTYPE html><html><head><meta charset="UTF-8">
<link href="//unpkg.com/swagger-ui-dist@3.22.1/swagger-ui.css" rel="stylesheet" type="text/css"/>
</head><body><div id="swagger-ui"></div>
<script src="//unpkg.com/swagger-ui-dist@3.22.1/swagger-ui-bundle.js"></script>
<script>SwaggerUIBundle({
  url: "/v1/oas3-spec.json",dom_id: '#swagger-ui',
  displayRequestDuration: true,
  presets: [
    SwaggerUIBundle.presets.apis,
    SwaggerUIBundle.SwaggerUIStandalonePreset
  ]})
</script>
</body></html>`);
  return res.end();
}
async function swaggerSpec(req, res) {
  const spec = yaml.safeLoad(
    await readFile(__dirname + '/veduz-1.1.0-swagger.yaml', 'utf-8')
  );
  res.json(spec);
  res.end();
}

const started = Date.now();
async function v1(req, res, next) {
  if (
    req.url.startsWith('/recommend') ||
    req.url.startsWith('/object') ||
    req.url.startsWith('/collection') ||
    req.url.startsWith('/randomPid') ||
    req.url.startsWith('/libraries')
  ) {
    bibHandler(req, res, next);
  } else if (req.url.startsWith('/geoip/')) {
    res.json(await geoip(req.url.replace('/geoip/', '')));
    res.end();
  } else if (req.url.startsWith('/stats/accesslog')) {
    res.json(
      await accessLogStats(req.url.replace('/stats/accesslog', ''))
    );
    res.end();
  } else if (req.url.startsWith('/stats/uptime')) {
    res.json((Date.now() - started) / 1000);
    res.end();
  } else if (req.url === '/oas3-spec.json') {
    await swaggerSpec(req, res);
  } else {
    swaggerUI(req, res);
  }
}
module.exports = {
  v1
};
