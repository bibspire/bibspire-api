const {Tail} = require('tail');
const qs = require('qs');
const moment = require('moment');
const {db} = require('./bibdata/db');
const {forLineInFile, sleep} = require('./util');

const table_name =
  process.env.NODE_ENV === 'production' ? 'stats' : 'statdev';

const sites = [
  '',
  '360-viewer.solsort.com',
  'annevoel.dk',
  'api.bibspire.dk',
  'app-tsartnoc.solsort.com',
  'art-quiz.solsort.com',
  'bibapp.solsort.com',
  'bibdata.dk',
  'bibspire.dk',
  'blobshot.solsort.com',
  'cdn.bibspire.dk',
  'dash.veduz.com',
  'forsider.dk',
  'frie-sange.solsort.com',
  'here.solsort.com',
  'installed-fonts.solsort.com',
  'js1k-2017.solsort.com',
  'js1k-compress.solsort.com',
  'js1k-rain.solsort.com',
  'js1k-sierpinsky.solsort.com',
  'minild-70.solsort.com',
  'mobibl.solsort.com',
  'mytekredsen.dk',
  'old-3d-photos.solsort.com',
  'other',
  'p2pweb.solsort.com',
  'rasmuserik.com',
  'rdf.solsort.com',
  'runicode.solsort.com',
  'single-touch-snake.solsort.com',
  'sketch-note-draw.solsort.com',
  'solsort.com',
  'solsort.dk',
  'stg.bibdata.dk',
  'te.solsort.com',
  'theodorelias.dk',
  'veduz.com',
  'voreskalender.dk',
  'weeklymovement.com',
  'www.bibspire.dk'
];

const methods = [
  'GET',
  'PUT',
  'POST',
  //'OPTIONS',
  //'CONNECT',
  //'PROPFIND',
  'other'
];
const platforms = [
  'android',
  'ipad',
  'iphone',
  'mac',
  'linux',
  'windows',
  'other'
];
const useragents = [
  'googlebot',
  'bingbot',
  'bot',
  'edge',
  'firefox',
  'chrome',
  'applewebkit',
  'trident',
  'other'
];
const referrers = [
  'none',
  'bibspire',
  'bibspire_userstatus',
  'bibspire_collection',
  'bibspire_other',
  'ting/object',
  'ting/collection',
  'user/me/status',
  'google',
  'baidu',
  'facebook',
  'bing',
  '.dk',
  '.com',
  'other'
];

function ENUM(list) {
  return `ENUM(${list.map(s => `'${s}'`).join(', ')})`;
}

const rerunStatistics = false;
async function startAccessLog() {
  if (rerunStatistics) {
    console.log('recreating statistics');
    console.time('recreating statistics');
    await db.execute(`DROP TABLE IF EXISTS ${table_name}`);
  }

  await db.execute(`CREATE TABLE IF NOT EXISTS ${table_name} (
        day DATE NOT NULL,
        site ${ENUM(sites)} NOT NULL,
        method ${ENUM(methods)} NOT NULL,
        path VARCHAR(63) NOT NULL,
        status SMALLINT NOT NULL,
        platform ${ENUM(platforms)} NOT NULL,
        useragent ${ENUM(useragents)} NOT NULL,
        referrer ${ENUM(referrers)} NOT NULL,
        count INT NOT NULL,
        ms INT NOT NULL,
        PRIMARY KEY(day, site, method, path, status, platform, useragent, referrer)
) CHARACTER SET 'utf8';`);

  if (rerunStatistics) {
    for (let i = 25; i > 1; --i) {
      await forLineInFile(
        `/var/log/nginx/access.log.${i}.gz`,
        processLine
      );
    }
    await forLineInFile('/var/log/nginx/access.log.1', processLine);
    await forLineInFile('/var/log/nginx/access.log', processLine);
    console.timeEnd('recreating statistics');
  }

  const tail = new Tail('/var/log/nginx/access.log', {
    follow: true
  });

  tail.on('line', function(line) {
    processLine(line);
  });
}
startAccessLog();

const lineRegEx = /([0-9.]+) [^ ]* [^ ]* \[([^\]]*)\] "([^ ]*) ?([^ "]*).*?" ([0-9]*) ([0-9]*) "([^"]*)" "([^"]*)" *([0-9.]*) *([^ ]*)/;
let currentStat = {};
let currentDay;
let lastSave;

async function addStat(
  day,
  {site, method, path, status, platform, useragent, referrer},
  ms
) {
  const key = JSON.stringify([
    site,
    method,
    path.slice(0, 63),
    status,
    platform,
    useragent,
    referrer
  ]);
  if (day !== currentDay || lastSave !== ((Date.now() / 10000) | 0)) {
    // note we will miss 0.1% of stat events due to concurrency, rewrite with proper locking to avoid this
    lastSave = (Date.now() / 10000) | 0;
    if (currentDay) {
      let rows = Object.keys(currentStat).map(key =>
        [currentDay].concat(JSON.parse(key)).concat(currentStat[key])
      );
      const maxRows = 5000;
      if (rows.length > maxRows) {
        console.log(
          'WARNING: to much statistics,',
          rows.length,
          'rows, truncating to',
          maxRows
        );
        for (let i = 0; i < 10; ++i) {
          console.log(
            'stat sample',
            JSON.stringify(rows[(Math.random() * rows.length) | 0])
          );
        }
        rows = rows.slice(0, maxRows);
      }
      if (rows.length) {
        await db.query(
          `REPLACE INTO ${table_name} 
      (day, site, method, path, status, platform, useragent, referrer, count, ms) VALUES ?;`, //(?, ?, ?, ?, ?, ?,?,?,?,?);`,
          [rows]
        );
      }
    }
    let result = (await db.query(
      `SELECT * FROM ${table_name} WHERE day=?`,
      [day]
    ))[0];
    currentStat = {};
    for (let row of result) {
      const {
        site,
        method,
        path,
        status,
        platform,
        useragent,
        referrer,
        count,
        ms
      } = row;
      const key = JSON.stringify([
        site,
        method,
        path.slice(0, 63),
        status,
        platform,
        useragent,
        referrer
      ]);
      currentStat[key] = [count, ms];
    }
    currentDay = day;
  }
  let [count, totalTime] = currentStat[key] || [0, 0];
  currentStat[key] = [count + 1, totalTime + ms];
}
let lineno = 0;
async function processLine(line) {
  try {
    let [
      unused,
      ip,
      date,
      method,
      path,
      status,
      length,
      referrer,
      useragent,
      time,
      site
    ] = line.match(lineRegEx);
    const isBot = useragent.toLowerCase().includes('bot');
    time = 1000 * +time;
    status = +status;
    date = moment(date, 'DD/MMM/YYYY:hh:mm:ss ZZ');
    path = path.toLowerCase();

    let platform;
    if (useragent.indexOf('Android') !== -1) {
      platform = 'android';
    } else if (useragent.indexOf('iPad') !== -1) {
      platform = 'ipad';
    } else if (useragent.indexOf('iPhone') !== -1) {
      platform = 'iphone';
    } else if (useragent.indexOf('Macintosh') !== -1) {
      platform = 'mac';
    } else if (useragent.indexOf('Linux') !== -1) {
      platform = 'linux';
    } else if (useragent.indexOf('Windows') !== -1) {
      platform = 'windows';
    } else {
      platform = 'other';
    }

    if (useragent.indexOf('Googlebot') !== -1) {
      useragent = 'googlebot';
    } else if (useragent.indexOf('bingbot') !== -1) {
      useragent = 'bingbot';
    } else if (useragent.match(/bot/i)) {
      useragent = 'bot';
    } else if (useragent.indexOf('Edge') !== -1) {
      useragent = 'edge';
    } else if (useragent.indexOf('Firefox') !== -1) {
      useragent = 'firefox';
    } else if (useragent.indexOf('Chrome') !== -1) {
      useragent = 'chrome';
    } else if (useragent.indexOf('AppleWebKit') !== -1) {
      useragent = 'applewebkit';
    } else if (useragent.indexOf('Trident') !== -1) {
      useragent = 'trident';
    } else {
      useragent = 'other';
    }

    if (!methods.includes(method)) {
      method = 'other';
    }

    path = path.replace(/^\/*/, '/');
    if (!sites.includes(site)) {
      path = site;
    } else if (path.startsWith('/object/')) {
      path = '/object/...';
    } else if (path.startsWith('/collection/')) {
      path = '/collection/...';
    } else if (path.startsWith('/cover/')) {
      path = '/cover/...';
    } else if (path.startsWith('/v1/thumb/')) {
      path = '/v1/thumb/...';
    } else if (path.startsWith('/_next/')) {
      path = '/_next/...';
      /*
    } else if(path.startsWith('/wp-admin')) {
      path = '/wp-admin/...'
    } else if(path.startsWith('/wp-includes/')) {
      path = '/wp-includes/...'
    } else if(path.startsWith('/wp-content/')) {
      path = '/wp-content/...'
      */
    } else if (path.startsWith('/wp')) {
      path = '/wp...';
    } else if (path.startsWith('/pid/')) {
      path = '/pid/...';
    } else if (path.startsWith('/work/')) {
      path = '/work/...';
    } else if (path.startsWith('/rss/')) {
      path = '/rss/...';
    } else if (path.startsWith('/search/')) {
      path = '/search/...';
    } else if (path.startsWith('/v1/object/')) {
      path = '/v1/object/...';
    } else if (path.startsWith('/v1/geoip/')) {
      path = '/v1/geoip/...';
    } else if (path.startsWith('/bibdata/ting/')) {
      path = '/bibdata/ting/...';
    } else if (path.startsWith('/bibdata/lid/')) {
      path = '/bibdata/lid/...';
    } else if (path.startsWith('/v1/bib/config')) {
      path = '/v1/bib/config';
    } else if (path.startsWith('/.well-known/')) {
      path = '/.well-known/...';
    } else if (path.startsWith('/series/')) {
      path = '/series/...';
    } else if (path.startsWith('/v1')) {
      path = path.replace(/(token=)[^&]*/g, (_, s) => s + '...');
      path = path.replace(/[&]pid=[^&]*/g, '');
      path = path.replace(/[?]pid=[^&]*[&]/g, '?');
      path = path.replace(/[?][&]/, '?');
    } else if (status === 404) {
      path = 'not found';
    } else {
      path = path.replace(/\?.*/, '?');
    }

    if (!sites.includes(site)) {
      site = 'other';
    }

    if (!referrer || referrer === '-') {
      referrer = 'none';
    } else if (referrer.indexOf('navigatedby=bibspire') !== -1) {
      referrer = referrer.replace(/.*navigatedby./, '');
      if (
        ![
          'bibspire',
          'bibspire_userstatus',
          'bibspire_collection'
        ].includes(referrer)
      ) {
        referrer = 'bibspire_other';
      }
    } else if (referrer.indexOf('ting/object') !== -1) {
      referrer = 'ting/object';
    } else if (referrer.indexOf('ting/collection') !== -1) {
      referrer = 'ting/collection';
    } else if (referrer.indexOf('user/me/status') !== -1) {
      referrer = 'user/me/status';
    } else if (referrer.indexOf('google.') !== -1) {
      referrer = 'google';
    } else if (referrer.indexOf('baidu.') !== -1) {
      referrer = 'baidu';
    } else if (referrer.indexOf('facebook.') !== -1) {
      referrer = 'facebook';
    } else if (referrer.indexOf('bing.') !== -1) {
      referrer = 'bing';
    } else if (referrer.indexOf('.dk') !== -1) {
      referrer = '.dk';
    } else if (referrer.indexOf('.com') !== -1) {
      referrer = '.com';
    } else {
      referrer = 'other';
    }

    if (++lineno % 1000 === 0) {
      //console.log(lineno);
      // site, method, path, status, platform, useragent, referrer
      //console.log(date.format('YYYY-MM-DD'), site, method, path, referrer, status, platform, useragent)
    }
    await addStat(
      date.format('YYYY-MM-DD'),
      {site, method, path, referrer, status, platform, useragent},
      Math.round(time)
    );
  } catch (e) {
    console.log(line);
    console.log(e);
  }
}

async function accessLogStats(arg) {
  const rows = (await db.query(`SELECT * FROM ${table_name}`))[0];

  await sleep(0);
  let result = {
    values: {
      sites,
      methods,
      platforms,
      useragents,
      referrers
    },
    dates: {}
  };

  let i = 0;
  for (const row of rows) {
    ++i;
    let {
      day,
      site,
      method,
      path,
      status,
      platform,
      useragent,
      referrer,
      count,
      ms
    } = row;
    day = moment(day).format('YYYY-MM-DD');
    result.dates[day] = result.dates[day] || [];
    result.dates[day].push([
      sites.indexOf(site),
      methods.indexOf(method),
      path,
      status,
      platforms.indexOf(platform),
      useragents.indexOf(useragent),
      referrers.indexOf(referrer),
      count,
      ms
    ]);
    if (i % 1000 === 0) {
      await sleep(0);
    }
  }
  return result;
}
module.exports = {accessLogStats};
