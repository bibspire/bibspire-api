cat /cloud/data/bib/bestand/* | sed -e 's/[^,]*,/870970-basis:/' | sed -e s/',.*//' > /tmp/pids
node -e 'pids = require("cbor").decode(require("fs").readFileSync(
"/cloud/data/bib/recommend/webtrek_pids.cbor"
));
for(const pid of pids) console.log(pid);
  ' >> /tmp/pids
node -e 'pids = require("cbor").decode(require("fs").readFileSync(
"/cloud/data/bib/recommend/adhl_pids.cbor"
));
for(const pid of pids) console.log(pid);
  ' >> /tmp/pids
cat /tmp/pids | sort | uniq | sort -R | gzip -9 > /cloud/data/bib/pids.gz
