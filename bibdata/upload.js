const CBOR = require('cbor');
const CRC32 = require('crc-32');
const {promisify} = require('util');
const readline = require('readline');
const _ = require('lodash');
const fs = require('fs');
const {normaliseName, sleep} = require('../util');
const {db} = require('./db');
const {pid2wid} = require('./metadata');

fs.mkdir(__dirname + '/incoming', () => {});
async function webtrekkUpload(req, res) {
  console.log('webtrekkUpload', req.query);
  const {token} = req.query;
  let current = (await db.query(
    'SELECT site FROM sites WHERE secret=?;',
    [token]
  ))[0][0];
  if (!current) {
    res.status(403);
    res.send('forbidden');
    return res.end();
  }
  const {site} = current;

  const filename = `${__dirname}/incoming/webtrekk.${normaliseName(
    site
  )}.${new Date()
    .toISOString()
    .replace(/\D/g, '')
    .slice(0, 12)}.csv`;
  const f = await promisify(fs.open)(filename, 'w');
  req.on('data', data => {
    promisify(fs.write)(f, data);
  });
  req.on('end', async data => {
    await promisify(fs.close)(f);
    res.send('saved');
    res.end();
  });
}
module.exports = {holdings, webtrekkUpload};

// TO BE DEPRECATED:
async function holdings(req, res) {
  const agency = +req.query.agency;
  const token = req.query.token;
  const expectedToken =
    agency.toString(36) + (agency / 1000000).toString(36).slice(2);
  if (token !== expectedToken) {
    res.status(401);
    res.send('invalid token');
    return res.end();
  }

  const f = await promisify(fs.open)(
    `${__dirname}/incoming/holdings.${agency}.csv`,
    'w'
  );
  req.on('data', data => {
    promisify(fs.write)(f, data);
  });
  req.on('end', data => {
    promisify(fs.close)(f);
    res.send('saved');
    res.end();
    loadHoldings(agency);
  });
}
async function holdings(req, res) {
  const agency = +req.query.agency;
  const token = req.query.token;
  const expectedToken =
    agency.toString(36) + (agency / 1000000).toString(36).slice(2);
  if (token !== expectedToken) {
    res.status(401);
    res.send('invalid token');
    return res.end();
  }

  const f = await promisify(fs.open)(
    `${__dirname}/incoming/holdings.${agency}.csv`,
    'w'
  );
  req.on('data', data => {
    promisify(fs.write)(f, data);
  });
  req.on('end', data => {
    promisify(fs.close)(f);
    res.send('saved');
    res.end();
    loadHoldings(agency);
  });
}
async function loadHoldings(agency) {
  const filename = `${__dirname}/incoming/holdings.${agency}.csv`;
  const fileStream = fs.createReadStream(filename);
  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });

  let i = 0;
  let t0 = Date.now();
  let rows = [];
  async function insert() {
    const values = rows;
    rows = [];
    if (values.length) {
      await db.query(
        'INSERT IGNORE INTO holdings (agency, branch, pid, wid) VALUES ?',
        [values]
      );
    }
    console.log(
      `${agency} inserted ${values.length} holdings in ${Date.now() -
        t0} ms, total: ${i}`
    );
    t0 = Date.now();
  }
  for await (const line of rl) {
    if (++i % 1000 === 0) {
      await insert();
    }
    let [branch, faust] = line.split(',');
    const pid = '870970-basis:' + faust;
    try {
      const wid = await pid2wid(pid);
      if (wid) {
        rows.push([agency, +branch, pid, wid]);
      } else {
        console.log(pid, 'missing');
      }
    } catch (e) {
      console.log(pid, 'error loading wid for holdings', String(e));
    }
  }
  await insert();
}
