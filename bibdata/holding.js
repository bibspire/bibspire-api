const {promisify} = require('util');
const fs = require('fs');
const {db} = require('./db');
const {pid2wid, lid2pid} = require('./metadata');
const {sleep} = require('../util');

// Schema:
//
// OnShelf, OnLoan, NotForLoan, Online, OnOrder
//

let running = false;
async function updateHoldings() {
  if (running) {
    return;
  }
  running = true;
  let maxSleep = 24 * 60 * 60 * 1000;
  // maxSleep = 0;
  await sleep(Math.random() * maxSleep);
  for (;;) {
    try {
      let allHoldings = JSON.parse(
        await promisify(fs.readFile)(
          '/data/holdings/holdings.json',
          'utf-8'
        )
      );
      const holdingTypes = {};
      const agencies = Object.keys(allHoldings);

      for (const agency of agencies) {
        console.time('Loading holdings for ' + agency);
        const holdingsArr = allHoldings[agency];
        const holdings = {};
        let lids = (await db.query(
          'SELECT lid FROM holding WHERE agency=?',
          [agency]
        ))[0].map(o => o.lid);

        for (const lid of lids) {
          holdings[lid] = {};
        }

        for (const [lid, state] of holdingsArr) {
          const o = holdings[lid] || {};
          holdings[lid] = o;
          o[state] = (o[state] || 0) + 1;
          holdingTypes[state] = (holdingTypes[state] || 0) + 1;
        }

        let rows = [];
        let i = 0;
        lids = Object.keys(holdings);
        for (const lid of lids) {
          const o = holdings[lid];
          if (++i % 10000 === 0) {
            console.log(
              `Loading holdings for ${agency}, did ${i}/${lids.length}`
            );
          }
          const wid = await pid2wid(lid2pid(lid, agency));
          if (!wid) {
            continue;
          }
          rows.push([
            agency,
            wid,
            lid,
            o.OnShelf || 0,
            o.OnLoan || 0,
            o.NotForLoan || 0,
            o.Online || 0,
            o.OnOrder || 0
          ]);
        }
        try {
          for (i = 0; i < rows.length; i += 10000) {
            await db.query(
              'REPLACE INTO holding (agency, wid, lid, onshelf, onloan, notforloan, online, onorder) VALUES ?;',
              [rows.slice(i, i + 10000)]
            );
          }
        } catch (e) {
          console.log('ERROR replacing into holdings for', agency);
        }
        try {
          await db.execute(
            'DELETE FROM holding WHERE agency=? AND onshelf=0 AND onloan=0 and notforloan=0 AND online=0 AND onorder=0',
            [agency]
          );
        } catch (e) {
          console.log('ERROR cleaning holdings for', agency);
        }
        console.timeEnd('Loading holdings for ' + agency);
      }
      await sleep(Math.random() * maxSleep);
    } catch (e) {
      console.log(e);
      // retry
    }
  }
}
updateHoldings();

async function availableAgencies() {
  return (await db.query(
    'SELECT DISTINCT agency FROM holding;'
  ))[0].map(o => o.agency);
}
async function agencyWorkPids(agency, wid) {
  return (await db.query(
    'SELECT lid FROM holding WHERE agency=? AND wid=?;',
    [agency, wid]
  ))[0].map(o => lid2pid(o.lid));
}

module.exports = {
  availableAgencies,
  agencyWorkPids
};

async function printAgencies() {
  const agencies = (await db.query(
    'SELECT DISTINCT agency FROM sites WHERE agency>0;'
  ))[0].map(o => String(o.agency));
  agencies.sort();
  console.log(JSON.stringify(agencies));
}
printAgencies();
