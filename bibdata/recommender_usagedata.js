const {db} = require('./db');
const {cached} = require('../util');
const _ = require('lodash');

const get_sids = cached('usagedata_sids', 1, async wid =>
  (await db.query(
    'SELECT d, sid FROM usagedata WHERE wid=? ORDER BY d DESC LIMIT 1000;',
    [wid]
  ))[0].map(o => o.sid)
);
const get_wids = cached('usagedata_wids', 1, async sid =>
  (await db.query('SELECT wid FROM usagedata WHERE sid=?;', [
    sid
  ]))[0].map(o => o.wid)
);

const popularity = new Map();
db.query(
  'SELECT wid, COUNT(*) AS count FROM usagedata GROUP BY wid;'
).then(res => {
  res[0].forEach(
    ({wid, count}) => count > 1 && popularity.set(wid, count)
  );
  console.log('usagedata popularity initialised');
});
const get_popularity = wid => popularity.get(wid) || 1;

module.exports = {
  get_wids,
  get_sids,
  get_popularity
};
