const fs = require('fs');
const readline = require('readline');
const CRC32 = require('crc-32');
const {pid2wid, getWork, workCount} = require('./metadata');
const {db} = require('./db');
const {sleep, cached} = require('../util');
const _ = require('lodash');

const get_sids = cached('webtrekk_sids', 1, async wid =>
  (await db.query('SELECT sid FROM webtrekk WHERE wid=? LIMIT 1000;', [
    wid
  ]))[0].map(o => o.sid)
);
const get_wids = cached('webtrekk_wids', 1, async sid =>
  (await db.query('SELECT wid FROM webtrekk WHERE sid=? LIMIT 1000;', [
    sid
  ]))[0].map(o => o.wid)
);
const popularity = new Map();
db.query(
  'SELECT wid, COUNT(*) AS count FROM webtrekk GROUP BY wid;'
).then(res => {
  res[0].forEach(
    ({wid, count}) => count > 1 && popularity.set(wid, count)
  );
  console.log('webtrekk popularity initialised');
});
const get_popularity = wid => popularity.get(wid) || 1;

module.exports = {
  get_wids,
  get_sids,
  get_popularity
};

//
// data initialisation
//
async function insertWebtrekk(filename) {
  console.time('insertWebtrekk ' + filename);
  const fileStream = fs.createReadStream(filename);
  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });

  let i = 0;
  let t0 = Date.now();
  let rows = [];
  async function insert() {
    const values = rows;
    rows = [];
    await db.query('INSERT IGNORE INTO webtrekk (sid, wid) VALUES ?;', [
      values
    ]);
    console.log(
      `webtrekk inserted ${values.length} in ${Date.now() -
        t0} ms, total: ${i}`
    );
    t0 = Date.now();
  }
  for await (const line of rl) {
    if (++i % 5000 === 0) {
      await insert();
    }

    const [session, object] = line.split('\t');
    sid = CRC32.str(session);
    pid = (object || '').replace(/^.*[.]/, '').replace(/[?#].*/, '');
    if (
      !pid ||
      pid.indexOf(':') === -1 ||
      pid.startsWith('publizon') ||
      pid.startsWith('query:')
    ) {
      continue;
    }

    try {
      const wid = await pid2wid(pid);
      if (!wid) {
        continue;
      }

      rows.push([sid, wid]);
    } catch (e) {
      console.log('webtrekk error fetching pid:', pid);
    }
  }
  await insert();
  console.timeEnd('insertWebtrekk ' + filename);
}
async function cleanupWebtrekk() {
  console.log('cleanup webtrekk');
  console.time('cleanup webtrekk');
  await db.execute(
    'CREATE OR REPLACE TABLE webtrekk_cleanup SELECT sid, COUNT(wid) AS wids FROM webtrekk GROUP BY sid;'
  );
  await db.execute('DELETE FROM webtrekk_cleanup WHERE wids>1;');
  await db.execute(
    'DELETE webtrekk, webtrekk_cleanup FROM webtrekk INNER JOIN webtrekk_cleanup WHERE webtrekk_cleanup.sid=webtrekk.sid AND webtrekk_cleanup.wids=1;'
  );
  await db.execute('DROP TABLE webtrekk_cleanup;');
  console.timeEnd('cleanup webtrekk');
}
(async () => {
  /*
  await sleep(3000);
const files = [
    'webtrekk.webtrekk.201909171020.csv',
    'webtrekk.webtrekk.201906251227.csv',

  'usagedata.2018-05-15.csv',
    'usagedata.2018-06-15.csv',
    'usagedata.2018-07-15.csv',
    'usagedata.2018-08-15.csv',
    'usagedata.2018-09-15.csv',
    'usagedata.2018-10-15.csv',
    'usagedata.2018-11-15.csv',
    'usagedata.2018-12-15.csv',
    'usagedata.2019-01-15.csv',
    'usagedata.2019-02-15.csv',
    'usagedata.2019-03-15.csv',
    'usagedata.2019-04-15.csv',
];
  for(const file of files) {
    console.log('loading ' + file);
    await insertWebtrekk(__dirname +'/incoming/' + file);
    cleanupWebtrekk();
  }

  console.time('webtrekk flush cache');
  console.timeEnd('webtrekk flush cache');
    */
})();
