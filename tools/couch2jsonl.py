import json
import sys

for line in sys.stdin:                
    line = line.strip();
    if line[-1] == ",":
        line = line[0:-1]
    try:
        o = json.loads(line)["doc"];
        o.pop('recommend', None);
        o.pop('_rev', None);
    except:
        continue
    if o.get("dcTitle", [""])[0].startswith('Error: unknown/missing/inaccessible record: '):
        o = {"_id": o["_id"], "unavailable": True}
    print(json.dumps(o));

